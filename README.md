# Tensorflow 1 Interface

This library contains the functions to convert TensorFlow 1 tensors into Icy sequences and viceversa. This library is compatible with TensorFlow versions up to 1.15